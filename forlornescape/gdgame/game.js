class Game {

  //#region Fields
  //canvas and context
  screenBottom;
  screenTop;

  //game resources
  spriteSheet;

  //time object and notification 
  gameTime;
  notificationCenter;

  //managers
  objectManager;
  soundManager;
  gameStateManager;
  cameraManager;

  //multi-player count and ready test
  playerSprites = new Array();
  readyPlayers = 0;
  maxPlayers = 2;

  //debug
  debugModeOn;
  //#endregion

  //#region Constructor
  constructor(debugModeOn) {
    //enable/disable debug info
    //this.debugModeOn = debugModeOn;
  }
  //#endregion

  //#region DEMO
  DemoDatabase() {

    let theData = null;

    /****************** DEMO - READ ALL ******************/
    this.DemoDBReadAll();

    /****************** DEMO - CREATE ONE ******************/
    //loadup the data
    theData = {
      game: "Forlorn Escape",
      player: "My Player Name",
      score: 80
    };
    // this.DemoCreateOne(theData);

    /****************** DEMO - UPDATE ONE ******************/
    //loadup the data
    theData = {
      _id: "5eabfa78e9d6551f942f2a97",
      game: "New Name Screen Same",
      player: "My NEW Name",
      score: 999
    };

    //this.DemoUpdateOne(theData);

    /****************** DEMO - DELETE ONE ******************/
    //loadup the data
    theData = {
      _id: "5eac0da25c99bd0378e2f89a"
    };
    //this.DemoDeleteOne(theData);

  }

  DemoCreateOne(theData) {

    //setup the variables used by the example
    let options = null;
    let gdRest = new GDREST();

    //set the options to specify the method, content type and body
    options = {
      method: "POST",
      mode: "no-cors",
      body: JSON.stringify(theData)
    };

    //perform the request (just like we would do in POSTMAN)
    gdRest.getData("http://127.0.0.1:3000/create", options)
      //success
      .then((data) => {
        console.log("\nCREATE ONE RECORD...");
        console.log('Count :>> ', data.insertedCount);
        console.log('Id :>> ', data.insertedId);
      })
      //failure
      .catch(error => {
        //code to display an error here if you want?
      });
  }

  DemoDBReadAll() {
    //setup the variables used by the example
    let options = null;
    let gdRest = new GDREST();

    //set the options to specify the method and note that we dont have any body since we're GETTING not POSTING
    options = {
      method: "GET"
    };

    //perform the request (just like we would do in POSTMAN)
    gdRest.getData("http://127.0.0.1:3000/read/all", options)
      //success
      .then((data) => {
        console.log("\nREAD ALL RECORDS...");
        for (let record of data) {
          console.log("\nRecord:\n" + record._id);
          console.log(record.game);
          console.log(record.player);
          console.log(record.score);
        }
      })
      //failure
      .catch(error => {
        //code to display an error here if you want?
      });
  }

  DemoUpdateOne(theData) {
    //setup the variables used by the example
    let options = null;
    let gdRest = new GDREST();

    //set the options to specify the method, content type and body
    options = {
      method: "PUT",
      body: JSON.stringify(theData)
    };

    //perform the request (just like we would do in POSTMAN)
    gdRest.getData("http://127.0.0.1:3000/update/", options)
      //success
      .then((data) => {
        console.log("\UPDATE ONE RECORD...");
        console.log('new data :>> ', data.value);
      })
      //failure
      .catch(error => {
        //code to display an error here if you want?
        console.log('error :>> ', error);
      });
  }

  DemoDeleteOne(theData) {
    //setup the variables used by the example
    let options = null;
    let gdRest = new GDREST();

    //set the options to specify the method, content type and body
    options = {
      method: "DELETE",
      body: JSON.stringify(theData)
    };

    //perform the request (just like we would do in POSTMAN)
    gdRest.getData("http://127.0.0.1:3000/delete", options)
      //success
      .then((data) => {
        console.log("\DELETE ONE RECORD...");
        console.log('data :>> ', data);
      })
      //failure
      .catch(error => {
        //code to display an error here if you want?
      });
  }
  //#endregion

  // #region LoadGame, Start, Animate
  LoadGame() {

    //load content
    this.Initialize();

    //publish an event to pause the object manager (i.e. no update) and render manager (i.e. no draw) and show the menu
    NotificationCenter.Notify(
      new Notification(
        NotificationType.Menu,
        NotificationAction.ShowMenuChanged,
        [StatusType.Off, "menu-bottom", "menu-top"]
      )
    );

    //start timer - notice that it is called only after we loaded all the game content
    this.Start();
  }

  Start() {
    //runs in proportion to refresh rate
    this.gameTime = new GameTime();
    this.animationTimer = window.requestAnimationFrame(this.Animate.bind(this));
  }

  Animate(now) {
    this.gameTime.Update(now);
    this.Update(this.gameTime);
    this.Draw(this.gameTime);
    window.requestAnimationFrame(this.Animate.bind(this));
  }
  // #endregion

  // #region Update, Draw
  Update(gameTime) {
    //update all the game sprites
    this.objectManager.Update(this.gameTime);

    //updates the camera manager which in turn updates all cameras
    this.cameraManager.Update(gameTime);

    //DEBUG - REMOVE LATER
    if (this.debugModeOn)
      this.debugDrawer.Update(gameTime);

  }

  Draw(gameTime) {
    //clear screen on each draw of ALL sprites (i.e. menu and game sprites)
    this.ClearScreen(this.screenTop.clearScreenColor, this.screenTop.cvs, this.screenTop.ctx, this.screenTop.topLeft);
    this.ClearScreen(this.screenBottom.clearScreenColor, this.screenBottom.cvs, this.screenBottom.ctx, this.screenBottom.topLeft);

    //draw all the game sprites
    this.renderManager.Draw(gameTime);

    //DEBUG - REMOVE LATER
    if (this.debugModeOn)
      this.debugDrawer.Draw(gameTime);
  }

  ClearScreen(color, canvasObject, context, topLeft) {
    context.save();
    context.fillStyle = color;
    context.fillRect(topLeft.x, topLeft.y, canvasObject.clientWidth, canvasObject.clientHeight);
    context.restore();
  }
  // #endregion

  /************************************************************ YOUR GAME SPECIFIC UNDER THIS POINT ************************************************************/
  // #region Initialize, Load(Debug, Cameras, Managers)
  Initialize() {
    this.LoadCanvases();
    this.LoadAssets();
    this.LoadNotificationCenter();
    this.LoadInputAndCameraManagers();
    //this.LoadCameras(); //make at the end as 1+ behaviors in camera may depend on sprite
    this.LoadAllOtherManagers();
    this.LoadSprites();

    //DEBUG - REMOVE LATER
    if (this.debugModeOn)
      this.LoadDebug();

  }

  LoadCanvases() {
    //get a handle to our context
    this.screenTop = GDGraphics.GetScreenObject(1, "parent-top", "canvas-top", "player-intro-top", "player-ui-top",
      new Vector2(0, 0), new Vector2(840, 346), Color.Black);
    this.screenBottom = GDGraphics.GetScreenObject(2, "parent-bottom", "canvas-bottom", "player-intro-bottom", "player-ui-bottom",
      new Vector2(0, 0), new Vector2(840, 346), Color.Black);
  }

  GetTargetPlayer(playerIndex) {
    if (playerIndex >= 0 && playerIndex < this.playerSprites.length)
      return this.playerSprites[playerIndex];
    else
      throw "Error: A behavior (e.g. TrackTargetTranslationBehavior) is looking for a player index that does not exist. Are there sufficient sprites in the playerSprites array?";
  }

  LoadCameras() {
    //#region Camera 1    
    let transform = new Transform2D(
      new Vector2(420, 173),//420, 173
      0,
      new Vector2(1, 1),
      new Vector2(this.screenTop.dimensions.x / 2, this.screenTop.dimensions.y / 2),
      new Vector2(this.screenTop.dimensions.x, this.screenTop.dimensions.y));

    let cameraTop = new Camera2D(
      "camera top",
      ActorType.Camera,
      transform,
      StatusType.IsUpdated,
      this.screenTop.ctx
    );

    /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/
    cameraTop.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

    /**************** NEED TO ADD A BEHAVIOR TO MAKE THIS A CONTROLLABLE ACTOR ***********/
    cameraTop.AttachBehavior(new TrackTargetTranslationBehavior(this, 0, new Vector2(0, 30)));//120

    this.cameraManager.Add(cameraTop);
    //#endregion

    //#region Camera 2
    transform = new Transform2D(
      new Vector2(430, 1213),
      0,
      new Vector2(1, 1),
      new Vector2(this.screenBottom.dimensions.x / 2, this.screenBottom.dimensions.y / 2),
      new Vector2(this.screenBottom.dimensions.x, this.screenBottom.dimensions.y)
    );

    let cameraBottom = new Camera2D(
      "camera bottom",
      ActorType.Camera,
      transform,
      StatusType.IsUpdated,
      this.screenBottom.ctx
    );

    /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/
    cameraBottom.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

    /**************** NEED TO ADD A BEHAVIOR TO MAKE THIS A CONTROLLABLE ACTOR ***********/
    cameraBottom.AttachBehavior(new TrackTargetTranslationBehavior(this, 1, new Vector2(0, 30)));

    this.cameraManager.Add(cameraBottom);
    //#endregion

  }

  LoadNotificationCenter() {
    this.notificationCenter = new NotificationCenter();
  }

  LoadInputAndCameraManagers() {
    //checks for keyboard input
    this.keyboardManager = new KeyboardManager();
    //stores the cameras in our game
    this.cameraManager = new CameraManager("stores and manages cameras");
  }

  LoadAllOtherManagers() {
    //update objects
    this.objectManager = new ObjectManager(
      "game sprites",
      StatusType.IsUpdated,
      this.cameraManager,
      this.notificationCenter
    );

    //draw objects
    this.renderManager = new RenderManager(
      "draws sprites in obj manager",
      StatusType.IsDrawn,
      this.objectManager,
      this.cameraManager,
      this.notificationCenter);

    //#region DOM related Intro & Loader functionality  
    //sentinel used to control if both players have pressed the Start button - value of 2 because we need two players to start!
    this.startGameSentinel = new NumericSentinel(2);

    //load a menu managers for each screen since they need to function independently
    this.menuManagerTop = new MyMenuManager(this, "menu-top", this.notificationCenter, this.keyboardManager,
      this.screenTop, this.startGameSentinel);

    this.menuManagerBottom = new MyMenuManager(this, "menu-bottom", this.notificationCenter, this.keyboardManager,
      this.screenBottom, this.startGameSentinel);
    //#endregion

    //audio - step 3 - instanciate the sound manager with the array of cues
    this.soundManager = new SoundManager(
      audioCueArray,
      this.notificationCenter);

    //add the manager that listens for events (win, lose, respawn) within the game and reacts
    this.gameStateManager = new MyGameStateManager("listens for game state changes",
        //notice that its starts off, just like the ObjectManager and RenderManager - if it didnt then we'd see the player UIs with menu
        StatusType.Off, 
        //used to add new sprites to the object manager
        this.objectManager, 
        //used to get a camera and its context
        this.cameraManager, 
        //used to listed for notifications
        this.notificationCenter, 
          //an array that stores the archetypes to be used by the game state manager
          this.GetGameStateManagerArchetypeArray(),
            //an array of the objects that contain information that this class needs to process and show UI notifications
            [this.screenTop, this.screenBottom]);
  }

  /**
   * Loads and return an array of all the sprites used by the MyGameStateManager
   *
   * @returns Array of sprites
   * @memberof Game
   */
  GetGameStateManagerArchetypeArray() {
    let sprite = null;
    let archetypeArray = new Array();

    return archetypeArray;
  }

  LoadDebug() {
    this.debugDrawer = new DebugDrawer("shows debug info", StatusType.IsDrawn,
      this.objectManager, this.cameraManager,
      this.notificationCenter,
      DebugDrawType.ShowDebugText | DebugDrawType.ShowSpriteCollisionPrimitive);
  }
  //#endregion

  //#region Load(Assets, Sprites)
  LoadAssets() {

  }

  LoadSprites() {
    let sprite = null;

    //removes any existing sprites in the manager as this may not be the first time the game is played
    this.objectManager.Clear();

    //load the level walls etc
    this.LoadMultipleSpritesFrom2DArray(LEVEL_ARCHITECTURE_DATA, new Vector2(0, 0));

    //load all the pickups
    this.LoadMultipleSpritesFrom2DArray(LEVEL_PICKUPS_DATA, new Vector2(0, 0));

    //this.LoadCarSprite()
  }

  LoadAnimatedPlayerSprite(theObject, translationOffset) {

    let artist = new AnimatedSpriteArtist(theObject);
    artist.SetTake(theObject.defaultTakeName);

    let transform = new Transform2D(
      Vector2.Add(theObject.translation, translationOffset),
      theObject.rotationInRadians,
      theObject.scale,
      theObject.origin,
      artist.GetSingleFrameDimensions(theObject.defaultTakeName));

    let sprite = new MoveableSprite(theObject.id,
      theObject.actorType,
      theObject.collisionProperties.type,
      transform, artist,
      theObject.statusType,
      theObject.scrollSpeedMultiplier,
      theObject.layerDepth);

    /**************** NEED TO FRICTION TO MAKE THIS CHARACTER MOVE IN A MORE BELIEVEABLE MANNER ***********/
    sprite.Body.MaximumSpeed = theObject.moveProperties.maximumSpeed;
    sprite.Body.Friction = theObject.moveProperties.frictionType;
    sprite.Body.Gravity = theObject.moveProperties.gravityType; //top-down, so no gravity in +Y direction

    /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/
    //assign a circular collision primitive to better fit the drawn sprite
    if (theObject.collisionProperties.primitive == CollisionPrimitiveType.Circle) {
      sprite.CollisionPrimitive = new CircleCollisionPrimitive(transform, theObject.collisionProperties.circleRadius);
    } else {
      sprite.CollisionPrimitive = new RectCollisionPrimitive(transform, theObject.collisionProperties.explodeRectangleBy);
    }

    /**************** NEED TO ADD A BEHAVIOR TO MAKE THIS A CONTROLLABLE ACTOR ***********/
    if(theObject.id == "freddy" || theObject.id == "jason" || theObject.id == "michael" || theObject.id == "leatherface")
    {
      sprite.AttachBehavior(
        new KillerBehavior(
          this.keyboardManager,
          this.objectManager,
          theObject.moveProperties.moveKeys,
          theObject.moveProperties.lookDirection,
          theObject.moveProperties.moveSpeed,
          theObject.moveProperties.rotateSpeedInRadians));
    }
    else
    {
      sprite.AttachBehavior(
        new SurvivorBehavior(
          this,
          this.keyboardManager,
          this.objectManager,
          theObject.moveProperties.moveKeys,
          theObject.moveProperties.lookDirection,
          theObject.moveProperties.moveSpeed,
          theObject.moveProperties.rotateSpeedInRadians));
    }

    //return the player so that we can add all players to playerSprites array so that any camera targeting a player can get a handle to the player in this array
    return sprite;
  }

  LoadAnimatedSprite(theObject, translationOffset) {

    let artist = new AnimatedSpriteArtist(theObject);
    artist.SetTake(theObject.defaultTakeName);

    let transform = new Transform2D(
      Vector2.Add(theObject.translation, translationOffset),
      theObject.rotation,
      theObject.scale,
      theObject.origin,
      artist.GetSingleFrameDimensions(theObject.defaultTakeName));

    let sprite = new Sprite(theObject.id,
      theObject.actorType,
      theObject.collisionProperties.type,
      transform, artist,
      theObject.statusType,
      theObject.scrollSpeedMultiplier,
      theObject.layerDepth);

    /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/
    if (theObject.collisionProperties.primitive == CollisionPrimitiveType.Circle) {
      sprite.CollisionPrimitive = new CircleCollisionPrimitive(transform, theObject.collisionProperties.circleRadius);
    } else {
      sprite.CollisionPrimitive = new RectCollisionPrimitive(transform, theObject.collisionProperties.explodeRectangleBy);
    }

    /**************** NEED TO ADD A BEHAVIOR? ***********/
    return sprite;

  }

  LoadMultipleSpritesFrom2DArray(theObject, translationOffset) {
    let maxRows = theObject.levelLayoutArray.length;
    let maxCols = theObject.levelLayoutArray[0].length;
    let blockWidth = theObject.maxBlockWidth;
    let blockHeight = theObject.maxBlockHeight;
    let transform = null;
    let artist = null;
    let sprite = null;

    for (let row = 0; row < maxRows; row++) {
      for (let col = 0; col < maxCols; col++) {
        //we read a number from the array (and subtract 1 because 0 is our draw nothing value)
        let levelSpritesNumber = theObject.levelLayoutArray[row][col];

        //if we get a value of 0 from the  we have nothing to draw
        if (levelSpritesNumber != 0) {
          transform = new Transform2D(
            Vector2.Add(new Vector2(col * blockWidth, row * blockHeight), translationOffset),
            theObject.levelSprites[levelSpritesNumber].rotation,
            theObject.levelSprites[levelSpritesNumber].scale,
            theObject.levelSprites[levelSpritesNumber].origin,
            theObject.levelSprites[levelSpritesNumber].sourceDimensions);

          //remember we can also add an animated artist instead
          artist = new SpriteArtist(theObject.levelSprites[levelSpritesNumber].spriteSheet,
            theObject.levelSprites[levelSpritesNumber].sourcePosition,
            theObject.levelSprites[levelSpritesNumber].sourceDimensions,
            theObject.levelSprites[levelSpritesNumber].alpha);

          sprite = new Sprite("block[" + row + "," + col + "]",
            theObject.levelSprites[levelSpritesNumber].actorType,
            theObject.levelSprites[levelSpritesNumber].collisionProperties.type,
            transform, artist,
            theObject.levelSprites[levelSpritesNumber].statusType,
            theObject.levelSprites[levelSpritesNumber].scrollSpeedMultiplier,
            theObject.levelSprites[levelSpritesNumber].layerDepth);

          /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/

          if (theObject.levelSprites[levelSpritesNumber].collisionProperties.primitive == CollisionPrimitiveType.Circle) {
            sprite.CollisionPrimitive = new CircleCollisionPrimitive(transform, theObject.levelSprites[levelSpritesNumber].collisionProperties.circleRadius);
          } else {
            sprite.CollisionPrimitive = new RectCollisionPrimitive(transform, theObject.levelSprites[levelSpritesNumber].collisionProperties.explodeRectangleBy);
          }

          //do we want to add behaviors if so then add them here?

          this.objectManager.Add(sprite);
        }
      }
    }
  }

  /*LoadCarSprite() {
    let theObject = CAR_DATA;
    let translationOffset = new Vector2(0, 0);

    let artist = new AnimatedSpriteArtist(theObject);
    artist.SetTake(theObject.defaultTakeName);

    let transform = new Transform2D(
      Vector2.Add(theObject.translation, translationOffset),
      theObject.rotationInRadians,
      theObject.scale,
      theObject.origin,
      artist.GetSingleFrameDimensions(theObject.defaultTakeName)
      );

    let sprite = new Sprite(theObject.id,
      theObject.actorType,
      theObject.collisionProperties.type,
      transform, artist,
      theObject.statusType,
      theObject.scrollSpeedMultiplier,
      theObject.layerDepth);

    sprite.CollisionPrimitive = new RectCollisionPrimitive(transform, theObject.collisionProperties.explodeRectangleBy);

    objectManager.Add(sprite);
  }*/
}

//instead of "load" we could use "DOMContentLoaded" but this would indicate load complete when the HTML and DOM is loaded and NOT when all styling, scripts and images have been downloaded
window.addEventListener("load", event => {
  let bDebugMode = false;
  let game = new Game(bDebugMode);
  game.LoadGame();
});




