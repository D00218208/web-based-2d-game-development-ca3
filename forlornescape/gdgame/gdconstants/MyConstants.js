//#region AUDIO DATA
//audio - step 2 - create an array with all cues
//note the name we use below MUST be identical to id used in HTML when loading the sound asset
const audioCueArray = [
  new AudioCue("background_music", AudioType.Menu, 1, 1, true, 0),
  new AudioCue("freddy_chase", AudioType.Menu, 1, 1, true, 0),
  new AudioCue("jason_chase", AudioType.Menu, 1, 1, true, 0),
  new AudioCue("michael_chase", AudioType.Menu, 1, 1, true, 0),
  new AudioCue("leatherface_chase", AudioType.Menu, 1, 1, true, 0),
  new AudioCue("stab_sound", AudioType.Pickup, 1, 1, false, 0)
  //new AudioCue("coin_pickup", AudioType.Pickup, 1, 1, false, 0)
  //add more cues here but make sure you load in the HTML!
];

//See Game::LoadAllOtherManagers() for SoundManager instanciation
//#endregion

//#region SPRITE DATA - LEVEL LAYOUT

/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
sourcePosition:             (x,y) texture space co-ordinates of the top-left corner of the sprite data in the spritesheet
sourceDimensions:           original un-scaled dimensions (w,h) of the sprite data in the spritesheet
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
*/
const LEVEL_ARCHITECTURE_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "level architecture data",
  levelSprites: {
    1: { //wall
      spriteSheet: document.getElementById("wall"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    2: { //door - left
      spriteSheet: document.getElementById("door-left"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    3: { //door - right
      spriteSheet: document.getElementById("door-right"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    4: { //door - front
      spriteSheet: document.getElementById("door-front"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    5: { //carpet
      spriteSheet: document.getElementById("carpet"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Decorator,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.NotCollidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    6: { //floorboard
      spriteSheet: document.getElementById("floorboard"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Decorator,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.NotCollidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    7: { //tile
      spriteSheet: document.getElementById("tile"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Decorator,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.NotCollidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    8: { //hedge
      spriteSheet: document.getElementById("hedge"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    9: { //grass
      spriteSheet: document.getElementById("grass"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Decorator,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.NotCollidable, 
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    }
  },
  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  levelLayoutArray: [
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1], 
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,1,1,1,1,1,1,1,1,7,1,1,1,1,1,1,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,1], 
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,1], 
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,1,1,1,1,1,1,1,1,6,1,1,1,1,1,1,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1], 
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,6,6,6,1,6,6,6,1,6,6,6,6,6,6,1],
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,6,6,6,1,6,6,6,1,6,6,6,6,6,6,1],  
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,1,6,1,1,6,6,6,1,6,6,6,6,6,6,1], 
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,1,6,1,1,6,6,6,1,6,6,6,6,6,6,1],
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,6,6,6,1,6,6,6,1,6,6,6,6,6,6,1],
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,6,6,6,1,6,6,6,1,6,6,6,6,6,6,1],
    [1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,5,5,5,1,6,6,6,1,6,6,6,1,6,6,6,6,6,6,1], 
    [1,1,1,1,1,1,6,1,1,1,1,1,1,1,1,1,5,5,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],  
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,1,1,1], 
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,1,6,1],
    [1,6,6,6,6,6,6,6,6,6,6,6,1,5,5,5,5,5,5,1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,2,9,3,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,1,6,6,6,6,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,1,6,6,6,6,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,8],
    [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
  ]
});

/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
sourcePosition:             (x,y) texture space co-ordinates of the top-left corner of the sprite data in the spritesheet
sourceDimensions:           original un-scaled dimensions (w,h) of the sprite data in the spritesheet
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
*/

const LEVEL_PICKUPS_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "level pickups data",
  levelSprites: {
    1: { //gas canister
      spriteSheet: document.getElementById("gas_canister"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    2: { //car keys
      spriteSheet: document.getElementById("car_keys"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    3: { //car tire
      spriteSheet: document.getElementById("car_tire"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    }
  },
  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  levelLayoutArray: [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
  ]
});

  const CAR_DATA = Object.freeze({
    id: "car data",
    spriteSheet: document.getElementById("car"),
    defaultTakeName: "idle",
    translation: new Vector2(500, 100),
    rotationInRadians: GDMath.ToRadians(90),
    scale: new Vector2(1, 1),
    origin: new Vector2(0, 0),
    actorType: ActorType.Player,
    statusType: StatusType.IsDrawn | StatusType.IsUpdated,
    scrollSpeedMultiplier: 1,
    layerDepth: 1,
    alpha: 1,
    collisionProperties: {
      type: CollisionType.Collidable,
      primitive: CollisionPrimitiveType.Rectangle,
      circleRadius: 0,
      explodeRectangleBy: 0,
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(150, 150), 
      cellData: [
        new Rect(0, 0, 150, 150)
      ]
    }
  });

//#region SPRITE DATA - ANIMATED PLAYERS
/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
defaultTakeName:            string name of the take to play when the animation is loaded
translation:                translation used to position the object on the screen
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
moveProperties:             defines fields related to movement of the sprite (e.g. initial look direction, move speed, rotate speed, gravity, friction, max speed)
takes:                      a compound object (takes) containing a set of key-value pairs representing the take name (e.g. walk) and all the data related to that take (e.g. fps, start frame, end frame, bounding box, an array of rectangles indicating where the sprites are in the source sprite sheet)
*/
const FREDDY_DATA = Object.freeze({
  id: "freddy",
  spriteSheet: document.getElementById("freddy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(420, 85),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 24, 59, 81),
        new Rect(96, 24, 77, 81),
        new Rect(198, 25, 59, 81),
        new Rect(276, 25, 77, 81),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 24, 59, 81)//play frame where player stands repeatedly
      ]
    }
  }
});

const JASON_DATA = Object.freeze({
  id: "jason",
  spriteSheet: document.getElementById("jason_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(420, 85),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(197, 24, 70, 82),
        new Rect(276, 24, 77, 85),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
      ]
    }
  }
});

const MICHAEL_DATA = Object.freeze({
  id: "michael",
  spriteSheet: document.getElementById("michael_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(420, 85),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(198, 24, 59, 81),
        new Rect(276, 24, 77, 81),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82)
      ]
    }
  }
});

const LEATHERFACE_DATA = Object.freeze({
  id: "leatherface",
  spriteSheet: document.getElementById("leatherface_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(420, 85),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off,
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(197, 24, 67, 82),
        new Rect(276, 24, 85, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82)
      ]
    }
  }
});

const ASH_DATA = Object.freeze({
  id: "ash",
  spriteSheet: document.getElementById("ash_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(420, 85),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off,
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
        new Rect(96, 25, 77, 79),
        new Rect(199, 26, 57, 82),
        new Rect(276, 26, 77, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
      ]
    }
  }
});

const TOMMY_DATA = Object.freeze({
  id: "tommy",
  spriteSheet: document.getElementById("tommy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,

    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
        new Rect(96, 25, 77, 79),
        new Rect(199, 26, 57, 82),
        new Rect(276, 26, 77, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
      ]
    }
  }
});

const LAURIE_DATA = Object.freeze({
  id: "laurie",
  spriteSheet: document.getElementById("laurie_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
        new Rect(96, 28, 77, 73),
        new Rect(199, 29, 57, 73),
        new Rect(276, 29, 77, 73),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
      ]
    }
  }
});

const NANCY_DATA = Object.freeze({
  id: "nancy",
  spriteSheet: document.getElementById("nancy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off,
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
        new Rect(96, 28, 77, 73),
        new Rect(199, 29, 57, 73),
        new Rect(276, 29, 77, 73),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
      ]
    }
  }
});

const FREDDY_DATA_P2 = Object.freeze({
  id: "freddy",
  spriteSheet: document.getElementById("freddy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)),
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 24, 59, 81),
        new Rect(96, 24, 77, 81),
        new Rect(198, 25, 59, 81),
        new Rect(276, 25, 77, 81),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 24, 59, 81)
      ]
    }
  }
});

const JASON_DATA_P2 = Object.freeze({
  id: "jason",
  spriteSheet: document.getElementById("jason_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)),
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(197, 24, 70, 82),
        new Rect(276, 24, 77, 85),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
      ]
    }
  }
});

const MICHAEL_DATA_P2 = Object.freeze({
  id: "michael",
  spriteSheet: document.getElementById("michael_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(198, 24, 59, 81),
        new Rect(276, 24, 77, 81),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82)
      ]
    }
  }
});

const LEATHERFACE_DATA_P2 = Object.freeze({
  id: "leatherface",
  spriteSheet: document.getElementById("leatherface_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,

    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)),
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.06,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82),
        new Rect(96, 23, 77, 82),
        new Rect(197, 24, 67, 82),
        new Rect(276, 24, 85, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(18, 23, 59, 82)
      ]
    }
  }
});

const ASH_DATA_P2 = Object.freeze({
  id: "ash",
  spriteSheet: document.getElementById("ash_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
        new Rect(96, 25, 77, 79),
        new Rect(199, 26, 57, 82),
        new Rect(276, 26, 77, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
      ]
    }
  }
});

const TOMMY_DATA_P2 = Object.freeze({
  id: "tommy",
  spriteSheet: document.getElementById("tommy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
        new Rect(96, 25, 77, 79),
        new Rect(199, 26, 57, 82),
        new Rect(276, 26, 77, 82),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 25, 57, 79),
      ]
    }
  }
});

const LAURIE_DATA_P2 = Object.freeze({
  id: "laurie",
  spriteSheet: document.getElementById("laurie_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)),
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
        new Rect(96, 28, 77, 73),
        new Rect(199, 29, 57, 73),
        new Rect(276, 29, 77, 73),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
      ]
    }
  }
});

const NANCY_DATA_P2 = Object.freeze({
  id: "nancy",
  spriteSheet: document.getElementById("nancy_animations"),
  defaultTakeName: "walk",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(90),
  scale: new Vector2(0.5, 0.5),
  origin: new Vector2(35, 42),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    circleRadius: 10,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(1, 0)), 
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, 
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk" :  {
      fps: 6,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
        new Rect(96, 28, 77, 73),
        new Rect(199, 29, 57, 73),
        new Rect(276, 29, 77, 73),
      ]
    },
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, 
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(20, 28, 57, 73),
      ]
    }
  }
});

const TOAST_GET_READY_ANIMATION_DATA = Object.freeze({
  id: "get_ready_animation",
  spriteSheet: document.getElementById("get_ready_animation"),
  defaultTakeName: "ready",
  translation: new Vector2(200, 200),
  rotationInRadians: 0,
  scale: new Vector2(0.2, 0.2),
  origin: new Vector2(0, 0),
  actorType: ActorType.NonCollidableAnimatedDecorator,
  statusType: StatusType.Off,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.NotCollidable,
    primitive: CollisionPrimitiveType.None,
    circleRadius: 0,
    explodeRectangleBy: 0,
  },
  moveProperties: null, 
  takes: {  
    "ready" :  {
      fps: 20,
      maxLoopCount:   1, 
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(1378, 309), 
      cellData: [
        new Rect(0, 0, 1378, 309), 
        new Rect(1378, 0, 1378, 309), 
        new Rect(2756, 0, 1378, 309), 
        new Rect(4134, 0, 1378, 309), 
      ]
    }
  }
});

//#endregion


