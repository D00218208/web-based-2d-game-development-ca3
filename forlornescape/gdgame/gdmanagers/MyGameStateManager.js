/**
 * @author patrick nugent
 * @version 1.0
 * @class MyGameStateManager
 */

class MyGameStateManager extends GameStateManager {

    constructor(id, statusType, objectManager, cameraManager, notificationCenter, spriteArchetypeArray, screenObjectArray) {
        super(id);
        this.id = id;
        this.statusType = statusType;
        this.objectManager = objectManager;
        this.cameraManager = cameraManager;
        this.health = 100;
        this.parts = 0;
        this.time = 120;

        //an array that stores the archetypes to be used by the game state manager
        this.spriteArchetypeArray = spriteArchetypeArray;

        //an array of the objects that contain information that this class needs to process and show UI notifications (e.g. playerID, uiID) 
        //for example, when a notification is received which of the two game screens (and player states) does it affect?
        this.screenObjectArray = screenObjectArray;

        //listen for notifications including menu related to set the statustype of this manager appropriately (e.g. as with ObjectManager, RenderManager)
        this.notificationCenter = notificationCenter;
        this.RegisterForNotifications();

        //setup the UIs for the players listed in screenObjectArray
        this.InitializeAllUIs();
    }

    /**
     * Setup all the UIs for the players in the game
     *
     * @memberof MyGameStateManager
     */
    InitializeAllUIs() {
        for (let screenObject of this.screenObjectArray)
            this.InitializeDemoUI(screenObject.uiID);
    }


    /**
     * Creates a demo UI for each player in the screenObjectArray and adds the HTML elements (e.g. div_score) to the correct canvas
     * You should change this method to add whatever UI elements your game uses.
     * @param {*} uiID
     * @memberof MyGameStateManager
     */
    InitializeDemoUI(uiID) {

        //get a handle to the div that we use for adding UI elements for this particular canvas (e.g. player-ui-top)
        let container = document.querySelector("#" + uiID);
        //some value to start our score with
        let initialHealth = 100;
        //create element to store the score
        let div_health = document.createElement("div");
        //set text
        div_health.innerText = "Health: " + initialHealth;
        //now class for any effects e.g. fadein
        div_health.setAttribute("class", "ui-info");
        //now position
        div_health.setAttribute("style", "bottom: 1%; left: 1%;");
        //add to the container so that we actually see it on screen!
        container.appendChild(div_health);

        //some value to start our score with
        let initialParts = 0;
        //create element to store the score
        let div_parts = document.createElement("div");
        //set text
        div_parts.innerText = "Parts Collected: " + initialParts + "/3";
        //now class for any effects e.g. fadein
        div_parts.setAttribute("class", "ui-info");
        //now position
        div_parts.setAttribute("style", "bottom: 1%; right: 1%;");
        //add to the container so that we actually see it on screen!
        container.appendChild(div_parts);

        //some value to start our score with
        let initialTime = 120;
        //create element to store the score
        let div_time = document.createElement("div");
        //set text
        div_time.innerText = "Time remaining: " + initialTime;
        //now class for any effects e.g. fadein
        div_time.setAttribute("class", "ui-info");
        //now position
        div_time.setAttribute("style", "top: 1%; left: 1%;");
        //add to the container so that we actually see it on screen!
        container.appendChild(div_time);
    }

    //#region Notification Handling
    RegisterForNotifications() {

        //register for menu notification
        this.notificationCenter.Register(
            NotificationType.Menu,
            this,
            this.HandleNotification
        );

        //register for pickup notifications so we can play animations, change game state (e.g. win/lose or add points to player UI)
        this.notificationCenter.Register(
            NotificationType.GameState,
            this,
            this.HandleNotification
        );
    }

    HandleNotification(...argArray) {
        let notification = argArray[0];
        switch (notification.NotificationAction) {
            case NotificationAction.ShowMenuChanged:
                this.HandleShowMenuChanged(notification.NotificationArguments[0]);
                break;

            case NotificationAction.Pickup:
                this.HandlePickup(
                    notification.NotificationArguments[0], //value of the pickup (e.g. 5 points)
                    notification.NotificationArguments[1], //id of the animation decorator to play (e.g. "coin_pickup_decorator") which must be already stored by MyGameStateManager::spriteArchetypeArray
                    notification.NotificationArguments[2], //reference to player that picked it up so we can obtain player ID and know which canvas to update
                    notification.NotificationArguments[3]); //reference to the pickup sprite so that we can obtain its position and draw any animated decorator at that point
                break;

            default:
                break;
        }
    }

    HandleShowMenuChanged(statusType) {

        //set the status of the manager so that its Update() will execute (or not if StatusType.Off)
        this.statusType = statusType; //statusType to set this manager to (e.g. Off, IsDrawn, IsDrawn | IsUpdated)

        //set the UI to visible or hidden
        this.ToggleDisplayUIs(statusType);
    }

    ToggleDisplayUIs(statusType) {
        //loop through each of the two players and get the uiID of the DIV for that player
        for (let screenObject of this.screenObjectArray) {
            //if show then set display to none (i.e. hidden)
            if (statusType == StatusType.Off)           
                document.querySelector("#" + screenObject.uiID).setAttribute("style", "display: none;");
            //if show then set display to block (i.e. visible)
            else                                       
                document.querySelector("#" + screenObject.uiID).setAttribute("style", "display: block;");
        }
    }

    /**
     * Called when a behavior (e.g. SurvivorBehavior) publishes a notification with NotificationType.GameState and NotificationAction.Pickup
     *
     * @param {number} value Value of the pickup (e.g. 5 for ammo)
     * @param {string} decoratorID ID (found in spriteArchetypeArray) of the decorator to show when this pickup is collided with. 
     * @param {Sprite} parent Reference to the sprite (i.e. the player object) that collected the pickup
     * @param {Sprite} sprite Reference to the sprite that was collected
     * @memberof MyGameStateManager
     */
    HandlePickup(value, decoratorID, parent, sprite) {
        //show an animation (based on archetype provided to InitializeArchetypes())
        //this.ShowPickupAnimatedDecorator(decoratorID, sprite);

        //update the UI
        this.UpdateParts(value, parent, sprite);

        //update the game state
        //this.UpdateGameState(value, parent, sprite);
    }

    //#endregion


    /**
     * Called to add an animated decorator for a pickup to the object manager
     *
     * @param {number} value Value of the pickup (e.g. 5 for ammo)
     * @param {string} decoratorID ID (found in spriteArchetypeArray) of the decorator to show when this pickup is collided with. 
     * @param {Sprite} sprite Reference to the sprite that was collected so that we can obtain its translation
     * @memberof MyGameStateManager
     */
    ShowPickupAnimatedDecorator(decoratorID, sprite) {
        //clone the decorator from the archetypeArray
        let decorator = this.spriteArchetypeArray[decoratorID].Clone();
        //turn the decorator (i.e. the animated sprite on)
        decorator.statusType = StatusType.IsDrawn | StatusType.IsUpdated;
        //set the position of the sprite
        decorator.Transform2D.SetTranslation(Vector2.Subtract(sprite.transform2D.translation, sprite.transform2D.origin));
        //add to the object manager
        this.objectManager.Add(decorator);
    }

    EndGame(winner) {
        for (let screenObject of this.screenObjectArray)
        {
            let container = document.querySelector("#" + screenObject.uiID);
            //create element to store the score
            let div_result = document.createElement("div");
            //set text
            div_result.innerText = winner + " won with " + this.time + " seconds left";
            div_result.setAttribute("style", "bottom: 50%; left: 50%;");
            container.appendChild(div_result);

            setTimeout(function(){ 
                location.reload();
            }, 5000);
        }
    }

    UpdateParts(value) {
        this.parts = this.parts + value;
        if(this.parts >= 3)
        {
            this.parts = 3;
            this.EndGame("survivor");
        }
        for (let screenObject of this.screenObjectArray)
        {
            let container = document.querySelector("#" + screenObject.uiID);
            let div_parts = container.children[1];
            container.removeChild(div_parts);

            //create element to store the score
            let new_div_parts = document.createElement("div");
            //set text
            new_div_parts.innerText = "Parts Collected: " + this.parts + "/3";
            //now class for any effects e.g. fadein
            new_div_parts.setAttribute("class", "ui-info");
            //now position
            new_div_parts.setAttribute("style", "bottom: 1%; right: 1%;");
            //add to the container so that we actually see it on screen!
            container.appendChild(new_div_parts);
        }
    }

    UpdateHealth(value) {
        this.health = this.health + value;
        if(this.health <= 0)
        {
            this.health = 0;
            this.EndGame("killer");
        }
        for (let screenObject of this.screenObjectArray)
        {
            let container = document.querySelector("#" + screenObject.uiID);
            let div_health = container.children[0];
            container.removeChild(div_health);

            //create element to store the score
            let new_div_health = document.createElement("div");
            //set text
            new_div_health.innerText = "Health: " + this.health;
            //now class for any effects e.g. fadein
            new_div_health.setAttribute("class", "ui-info");
            //now position
            new_div_health.setAttribute("style", "bottom: 1%; left: 1%;");
            //add to the container so that we actually see it on screen!
            container.appendChild(new_div_health);
        }
    }

    UpdateTime(value) {
        this.time = this.time + value;
        if(this.time <= 0)
        {
            this.time = 0;
            this.EndGame("survivor");
        }
        for (let screenObject of this.screenObjectArray)
        {
            let container = document.querySelector("#" + screenObject.uiID);
            let div_time = container.children[2];
            container.removeChild(div_time);

            //create element to store the score
            let new_div_time = document.createElement("div");
            //set text
            new_div_time.innerText = "Time remaining: " + this.time;
            //now class for any effects e.g. fadein
            new_div_time.setAttribute("class", "ui-info");
            //now position
            new_div_time.setAttribute("style", "top: 1%; left: 1%;");
            //add to the container so that we actually see it on screen!
            container.appendChild(new_div_time);
        }
    }
}