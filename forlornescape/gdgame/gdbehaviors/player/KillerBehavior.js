/**
 * @author patrick nugent
 * @version 1.0
 * @class KillerBehavior
 */
class KillerBehavior extends Behavior{
    //#region Static Fields
    //#endregion
  
    //#region Fields
    //#endregion
  
    //#region Properties
    //#endregion
  
    constructor(
      keyboardManager,
      objectManager,
      moveKeys,
      originalDirection,
      moveSpeed = 2,
      rotateSpeed = 0.04
    ) {
      super();
      this.keyboardManager = keyboardManager;
      this.objectManager = objectManager;
  
      this.moveKeys = moveKeys;
      this.originalDirection = originalDirection.Clone(); //direction we were facing at game start
      this.lookDirection = originalDirection.Clone(); //direction after we turn
  
      this.moveSpeed = moveSpeed;
      this.rotateSpeed = rotateSpeed;
    }
  
    //#region Your Game Specific Methods - add code for more CD/CR or input handling
    HandleInput(gameTime, parent) {
      this.HandleMove(gameTime, parent);
    }
  
    HandleMove(gameTime, parent) {
  
      //up/down
      if (this.keyboardManager.IsKeyDown(this.moveKeys[0])) {
        //rotate the look direction used to move the sprite when forward/backward keys are pressed
        this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)
  
        //move forward using the look direction
        parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, gameTime.ElapsedTimeInMs * this.moveSpeed));
  
        //change the animation to something else (e.g. idle or use your own animation here)
        parent.Artist.SetTake("walk");
      } else if (this.keyboardManager.IsKeyDown(this.moveKeys[1])) {
        //rotate the look direction used to move the sprite when forward/backward keys are pressed
        this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)
  
        //move backward using the look direction
        parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, -gameTime.ElapsedTimeInMs * this.moveSpeed));
  
        //change the animation to something else (e.g. idle or use your own animation here)
        parent.Artist.SetTake("walk");
      }
  
      //turn left/right
      if (this.keyboardManager.IsKeyDown(this.moveKeys[2])) {
        //rotate the drawn sprite
        parent.Transform2D.RotateBy(GDMath.ToRadians(-4));
  
        //change the animation to something else (e.g. idle or use your own animation here)
        parent.Artist.SetTake("idle");
      } else if (this.keyboardManager.IsKeyDown(this.moveKeys[3])) {
        //rotate the drawn sprite
        parent.Transform2D.RotateBy(GDMath.ToRadians(4));
  
        //change the animation to something else (e.g. idle or use your own animation here)
        parent.Artist.SetTake("idle");
      }
    }
  
    CheckCollisions(parent) {
  
      this.HandleArchitectureCollision(parent);
  
      this.HandleEnemyCollision(parent);
  
      this.HandlePickupCollision(parent);
    }

    HandlePickupCollision(parent) {
    }
  
    HandleEnemyCollision(parent) {
  
      let sprites = this.objectManager.Get(ActorType.Player);
  
      for (let i = 0; i < sprites.length; i++) {
        let sprite = sprites[i];
  
        if (Collision.Intersects(parent, sprite)) {
          //stop the player from moving otherwise s/he will move through the enemy!
          parent.Body.SetVelocityX(0);
          parent.Body.SetVelocityY(0);
  
          //play sound
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, NotificationAction.Play, [
              "stab_sound"
            ]));
  
          //remove health
          NotificationCenter.Notify(
            new Notification(
              NotificationType.GameState,
              NotificationAction.Health,
              [-10]));
        }
      }
    }
  
    //#endregion
  
    //#region Core Methods - doesnt need to change
    Execute(gameTime, parent) {
      this.HandleInput(gameTime, parent);
      this.ApplyForces(parent);
      this.CheckCollisions(parent);
      this.ApplyInput(parent);
    }
  
    ApplyForces(parent) {
      //notice we need to slow body in X and Y and we dont ApplyGravity() in a top-down game
      parent.Body.ApplyFrictionX();
      parent.Body.ApplyFrictionY();
    }
  
    HandleArchitectureCollision(parent) {
      let sprites = this.objectManager.Get(ActorType.Architecture);
  
      for (let i = 0; i < sprites.length; i++) {
        let sprite = sprites[i];
        let collisionLocationType = Collision.GetIntersectsLocation(parent, sprite);
  
        //the code below fixes a bug which caused sprites to stick inside an object
        if (collisionLocationType === CollisionLocationType.Left) {
          if (parent.Body.velocityX <= 0)
            parent.Body.SetVelocityX(0);
        } else if (collisionLocationType === CollisionLocationType.Right) {
          if (parent.Body.velocityX >= 0)
            parent.Body.SetVelocityX(0);
        }
        //the code below fixes a bug which caused sprites to stick inside an object
        if (collisionLocationType === CollisionLocationType.Top) {
          if (parent.Body.velocityY <= 0)
            parent.Body.SetVelocityY(0);
        } else if (collisionLocationType === CollisionLocationType.Bottom) {
          if (parent.Body.velocityY >= 0)
            parent.Body.SetVelocityY(0);
        }
  
      }
    }
  
    ApplyInput(parent) {
      //if we have small left over values then zero
      if (Math.abs(parent.Body.velocityX) <= Body.MIN_SPEED)
        parent.Body.velocityX = 0;
      if (Math.abs(parent.Body.velocityY) <= Body.MIN_SPEED)
        parent.Body.velocityY = 0;
  
      //apply velocity to (x,y) of the parent's translation
      parent.Transform2D.TranslateBy(new Vector2(parent.Body.velocityX, parent.Body.velocityY));
    }
    //#endregion
  
    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
      //to do...
      throw "Not Yet Implemented";
    }
  
    ToString() {
      //to do...
      throw "Not Yet Implemented";
    }
  
    Clone() {
      //to do...
      throw "Not Yet Implemented";
    }
    //#endregion
  }