/**
 * Provides connection to a MongoDB specified in .env file
 * @author niall mcguinness 
 */

const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectID;

//save connection
const state = {
  db: null,
};

exports.connect = function (db_url, db_name, cb) {
  if (state.db) cb();
  else {
    MongoClient.connect(
      db_url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      },
      (err, client) => {
        if (err) cb(err);
        else {
          state.db = client.db(db_name);
          cb();
        }
      }
    );
  }
};

exports.getPrimaryKey = function (id) {
  return ObjectID(id);
};

exports.getDatabase = function () {
  return state.db;
};