/**
 * Represents the data to be stored in the MongoGO
 * @author niall mcguinness 
 */

const mongoose = require('mongoose');

//Mongoose ODM schema
const PlayerDataSchema = new mongoose.Schema({ 
    game: {  //game name
        type: String, 
        required: true,
        trim: true
    },
    //we can add other fields too but just ensure you update createOne and the JSON object you send with the new age field
    /*
    age: {
        type: Number,
        required: false,
        min: 18,
        max: 100,
        default: 18
    },
    */
    player: {  //player name
        type: String, 
        required: true,
        trim: true
    },
    score:{ //score!
        type: Number,
        required: true,
        min: -1000000,
        max:  1000000
    }, //entry date
    creationdate:{
        type: Date,
        default: Date.now()
    }

});

module.exports = mongoose.model('PlayerData', PlayerDataSchema);
